# LaTeX2HTML 2002-2-1 (1.71)
# Associate labels original text with physical files.


$key = q/cite_rossetti-1546/;
$external_labels{$key} = "$URL/" . q|node32.html|; 
$noresave{$key} = "$nosave";

$key = q/eq:cor2/;
$external_labels{$key} = "$URL/" . q|node15.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_bosch-2072/;
$external_labels{$key} = "$URL/" . q|node32.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_scheiner-5898/;
$external_labels{$key} = "$URL/" . q|node32.html|; 
$noresave{$key} = "$nosave";

$key = q/eq:potential/;
$external_labels{$key} = "$URL/" . q|node14.html|; 
$noresave{$key} = "$nosave";

$key = q/fig:punts_est/;
$external_labels{$key} = "$URL/" . q|node18.html|; 
$noresave{$key} = "$nosave";

$key = q/eq:bipot7/;
$external_labels{$key} = "$URL/" . q|node19.html|; 
$noresave{$key} = "$nosave";

$key = q/eq:schroringer/;
$external_labels{$key} = "$URL/" . q|node14.html|; 
$noresave{$key} = "$nosave";

$key = q/fig:splittings/;
$external_labels{$key} = "$URL/" . q|node23.html|; 
$noresave{$key} = "$nosave";

$key = q/fig:caspt2/;
$external_labels{$key} = "$URL/" . q|node28.html|; 
$noresave{$key} = "$nosave";

$key = q/eq:bipot1/;
$external_labels{$key} = "$URL/" . q|node19.html|; 
$noresave{$key} = "$nosave";

$key = q/eq:dvr_cinetic/;
$external_labels{$key} = "$URL/" . q|node14.html|; 
$noresave{$key} = "$nosave";

$key = q/fig:metil/;
$external_labels{$key} = "$URL/" . q|node22.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_sekiya-377/;
$external_labels{$key} = "$URL/" . q|node32.html|; 
$noresave{$key} = "$nosave";

$key = q/eq:wavefunc/;
$external_labels{$key} = "$URL/" . q|node14.html|; 
$noresave{$key} = "$nosave";

$key = q/fig:fun7_12D/;
$external_labels{$key} = "$URL/" . q|node24.html|; 
$noresave{$key} = "$nosave";

$key = q/eq:bipot6/;
$external_labels{$key} = "$URL/" . q|node19.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_makri-4026/;
$external_labels{$key} = "$URL/" . q|node32.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_ito-517/;
$external_labels{$key} = "$URL/" . q|node32.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_parr-785/;
$external_labels{$key} = "$URL/" . q|node32.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_becke-5648/;
$external_labels{$key} = "$URL/" . q|node32.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_bosch-5685/;
$external_labels{$key} = "$URL/" . q|node32.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_moller-618/;
$external_labels{$key} = "$URL/" . q|node32.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_sekiya-3961/;
$external_labels{$key} = "$URL/" . q|node32.html|; 
$noresave{$key} = "$nosave";

$key = q/mat_cinetic/;
$external_labels{$key} = "$URL/" . q|node14.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_roos-1218/;
$external_labels{$key} = "$URL/" . q|node32.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_PTReactions/;
$external_labels{$key} = "$URL/" . q|node32.html|; 
$noresave{$key} = "$nosave";

$key = q/eq:cor1/;
$external_labels{$key} = "$URL/" . q|node15.html|; 
$noresave{$key} = "$nosave";

$key = q/eq:bipot3/;
$external_labels{$key} = "$URL/" . q|node19.html|; 
$noresave{$key} = "$nosave";

$key = q/fig:fun7_12/;
$external_labels{$key} = "$URL/" . q|node24.html|; 
$noresave{$key} = "$nosave";

$key = q/fig:matH/;
$external_labels{$key} = "$URL/" . q|node14.html|; 
$noresave{$key} = "$nosave";

$key = q/eq:rot/;
$external_labels{$key} = "$URL/" . q|node20.html|; 
$noresave{$key} = "$nosave";

$key = q/eq:cor3/;
$external_labels{$key} = "$URL/" . q|node15.html|; 
$noresave{$key} = "$nosave";

$key = q/cinetic/;
$external_labels{$key} = "$URL/" . q|node14.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_molpro/;
$external_labels{$key} = "$URL/" . q|node32.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_salahub-4439/;
$external_labels{$key} = "$URL/" . q|node32.html|; 
$noresave{$key} = "$nosave";

$key = q/eq:cinpot/;
$external_labels{$key} = "$URL/" . q|node14.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_paz-8114/;
$external_labels{$key} = "$URL/" . q|node32.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_baughcum-2260/;
$external_labels{$key} = "$URL/" . q|node32.html|; 
$noresave{$key} = "$nosave";

$key = q/eq:schroringer2/;
$external_labels{$key} = "$URL/" . q|node14.html|; 
$noresave{$key} = "$nosave";

$key = q/eq:ordre/;
$external_labels{$key} = "$URL/" . q|node9.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_werner-5546/;
$external_labels{$key} = "$URL/" . q|node32.html|; 
$noresave{$key} = "$nosave";

$key = q/eq:sigma/;
$external_labels{$key} = "$URL/" . q|node14.html|; 
$noresave{$key} = "$nosave";

$key = q/fig:xz/;
$external_labels{$key} = "$URL/" . q|node20.html|; 
$noresave{$key} = "$nosave";

$key = q/fig:tropolona/;
$external_labels{$key} = "$URL/" . q|node8.html|; 
$noresave{$key} = "$nosave";

$key = q/fig:rotlevels/;
$external_labels{$key} = "$URL/" . q|node23.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_makri-1451/;
$external_labels{$key} = "$URL/" . q|node32.html|; 
$noresave{$key} = "$nosave";

$key = q/tab:ele/;
$external_labels{$key} = "$URL/" . q|node26.html|; 
$noresave{$key} = "$nosave";

$key = q/tab:spl/;
$external_labels{$key} = "$URL/" . q|node23.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_g98/;
$external_labels{$key} = "$URL/" . q|node32.html|; 
$noresave{$key} = "$nosave";

$key = q/eq:bipot4/;
$external_labels{$key} = "$URL/" . q|node19.html|; 
$noresave{$key} = "$nosave";

$key = q/eq:bipot2/;
$external_labels{$key} = "$URL/" . q|node19.html|; 
$noresave{$key} = "$nosave";

$key = q/fig:fun1_6/;
$external_labels{$key} = "$URL/" . q|node24.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_sekiya-4802/;
$external_labels{$key} = "$URL/" . q|node32.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_baughcum-6296/;
$external_labels{$key} = "$URL/" . q|node32.html|; 
$noresave{$key} = "$nosave";

$key = q/tab:comp/;
$external_labels{$key} = "$URL/" . q|node27.html|; 
$noresave{$key} = "$nosave";

$key = q/eq:bipot5/;
$external_labels{$key} = "$URL/" . q|node19.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_miller-1982/;
$external_labels{$key} = "$URL/" . q|node32.html|; 
$noresave{$key} = "$nosave";

$key = q/eq:rot3/;
$external_labels{$key} = "$URL/" . q|node20.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_sekiya-49/;
$external_labels{$key} = "$URL/" . q|node32.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_roos-5483/;
$external_labels{$key} = "$URL/" . q|node32.html|; 
$noresave{$key} = "$nosave";

$key = q/fig:mtropolona/;
$external_labels{$key} = "$URL/" . q|node12.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_stratmann-8218/;
$external_labels{$key} = "$URL/" . q|node32.html|; 
$noresave{$key} = "$nosave";

$key = q/fig:xy/;
$external_labels{$key} = "$URL/" . q|node19.html|; 
$noresave{$key} = "$nosave";

$key = q/eq:cart/;
$external_labels{$key} = "$URL/" . q|node14.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_light-1400/;
$external_labels{$key} = "$URL/" . q|node32.html|; 
$noresave{$key} = "$nosave";

$key = q/eq:ang/;
$external_labels{$key} = "$URL/" . q|node14.html|; 
$noresave{$key} = "$nosave";

$key = q/eq:rot2/;
$external_labels{$key} = "$URL/" . q|node20.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_Tunnel/;
$external_labels{$key} = "$URL/" . q|node32.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_paz-353/;
$external_labels{$key} = "$URL/" . q|node32.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_lapack/;
$external_labels{$key} = "$URL/" . q|node32.html|; 
$noresave{$key} = "$nosave";

$key = q/fig:fun1_6D/;
$external_labels{$key} = "$URL/" . q|node24.html|; 
$noresave{$key} = "$nosave";

$key = q/eq:dvr_potential/;
$external_labels{$key} = "$URL/" . q|node14.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_paz-6275/;
$external_labels{$key} = "$URL/" . q|node32.html|; 
$noresave{$key} = "$nosave";

$key = q/fig:splitting/;
$external_labels{$key} = "$URL/" . q|node9.html|; 
$noresave{$key} = "$nosave";

$key = q/eq:cor4/;
$external_labels{$key} = "$URL/" . q|node15.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_sekiya-1589/;
$external_labels{$key} = "$URL/" . q|node32.html|; 
$noresave{$key} = "$nosave";

$key = q/eq:pot/;
$external_labels{$key} = "$URL/" . q|node20.html|; 
$noresave{$key} = "$nosave";

1;


# LaTeX2HTML 2002-2-1 (1.71)
# labels from external_latex_labels array.


$key = q/eq:schroringer2/;
$external_latex_labels{$key} = q|1.3|; 
$noresave{$key} = "$nosave";

$key = q/eq:cor2/;
$external_latex_labels{$key} = q|1.15|; 
$noresave{$key} = "$nosave";

$key = q/eq:ordre/;
$external_latex_labels{$key} = q|1.1|; 
$noresave{$key} = "$nosave";

$key = q/eq:potential/;
$external_latex_labels{$key} = q|1.6|; 
$noresave{$key} = "$nosave";

$key = q/fig:xz/;
$external_latex_labels{$key} = q|3.3|; 
$noresave{$key} = "$nosave";

$key = q/eq:sigma/;
$external_latex_labels{$key} = q|1.13|; 
$noresave{$key} = "$nosave";

$key = q/fig:tropolona/;
$external_latex_labels{$key} = q|1.1|; 
$noresave{$key} = "$nosave";

$key = q/fig:rotlevels/;
$external_latex_labels{$key} = q|3.5|; 
$noresave{$key} = "$nosave";

$key = q/fig:punts_est/;
$external_latex_labels{$key} = q|3.1|; 
$noresave{$key} = "$nosave";

$key = q/eq:bipot7/;
$external_latex_labels{$key} = q|3.7|; 
$noresave{$key} = "$nosave";

$key = q/tab:ele/;
$external_latex_labels{$key} = q|3.2|; 
$noresave{$key} = "$nosave";

$key = q/fig:splittings/;
$external_latex_labels{$key} = q|3.6|; 
$noresave{$key} = "$nosave";

$key = q/tab:spl/;
$external_latex_labels{$key} = q|3.1|; 
$noresave{$key} = "$nosave";

$key = q/eq:schroringer/;
$external_latex_labels{$key} = q|1.2|; 
$noresave{$key} = "$nosave";

$key = q/fig:caspt2/;
$external_latex_labels{$key} = q|3.11|; 
$noresave{$key} = "$nosave";

$key = q/eq:bipot4/;
$external_latex_labels{$key} = q|3.4|; 
$noresave{$key} = "$nosave";

$key = q/eq:bipot1/;
$external_latex_labels{$key} = q|3.1|; 
$noresave{$key} = "$nosave";

$key = q/eq:bipot2/;
$external_latex_labels{$key} = q|3.2|; 
$noresave{$key} = "$nosave";

$key = q/eq:dvr_cinetic/;
$external_latex_labels{$key} = q|1.9|; 
$noresave{$key} = "$nosave";

$key = q/fig:metil/;
$external_latex_labels{$key} = q|3.4|; 
$noresave{$key} = "$nosave";

$key = q/fig:fun1_6/;
$external_latex_labels{$key} = q|3.7|; 
$noresave{$key} = "$nosave";

$key = q/eq:wavefunc/;
$external_latex_labels{$key} = q|1.12|; 
$noresave{$key} = "$nosave";

$key = q/tab:comp/;
$external_latex_labels{$key} = q|3.3|; 
$noresave{$key} = "$nosave";

$key = q/fig:fun7_12D/;
$external_latex_labels{$key} = q|3.10|; 
$noresave{$key} = "$nosave";

$key = q/eq:bipot6/;
$external_latex_labels{$key} = q|3.6|; 
$noresave{$key} = "$nosave";

$key = q/eq:bipot5/;
$external_latex_labels{$key} = q|3.5|; 
$noresave{$key} = "$nosave";

$key = q/eq:rot3/;
$external_latex_labels{$key} = q|3.9|; 
$noresave{$key} = "$nosave";

$key = q/fig:mtropolona/;
$external_latex_labels{$key} = q|1.3|; 
$noresave{$key} = "$nosave";

$key = q/fig:xy/;
$external_latex_labels{$key} = q|3.2|; 
$noresave{$key} = "$nosave";

$key = q/eq:cart/;
$external_latex_labels{$key} = q|1.10|; 
$noresave{$key} = "$nosave";

$key = q/mat_cinetic/;
$external_latex_labels{$key} = q|1.8|; 
$noresave{$key} = "$nosave";

$key = q/eq:ang/;
$external_latex_labels{$key} = q|1.11|; 
$noresave{$key} = "$nosave";

$key = q/eq:rot2/;
$external_latex_labels{$key} = q|3.10|; 
$noresave{$key} = "$nosave";

$key = q/eq:cor1/;
$external_latex_labels{$key} = q|1.14|; 
$noresave{$key} = "$nosave";

$key = q/eq:bipot3/;
$external_latex_labels{$key} = q|3.3|; 
$noresave{$key} = "$nosave";

$key = q/fig:fun7_12/;
$external_latex_labels{$key} = q|3.8|; 
$noresave{$key} = "$nosave";

$key = q/fig:matH/;
$external_latex_labels{$key} = q|1.4|; 
$noresave{$key} = "$nosave";

$key = q/eq:rot/;
$external_latex_labels{$key} = q|3.8|; 
$noresave{$key} = "$nosave";

$key = q/eq:cor3/;
$external_latex_labels{$key} = q|1.16|; 
$noresave{$key} = "$nosave";

$key = q/cinetic/;
$external_latex_labels{$key} = q|1.5|; 
$noresave{$key} = "$nosave";

$key = q/eq:dvr_potential/;
$external_latex_labels{$key} = q|1.7|; 
$noresave{$key} = "$nosave";

$key = q/fig:fun1_6D/;
$external_latex_labels{$key} = q|3.9|; 
$noresave{$key} = "$nosave";

$key = q/fig:splitting/;
$external_latex_labels{$key} = q|1.2|; 
$noresave{$key} = "$nosave";

$key = q/eq:cor4/;
$external_latex_labels{$key} = q|1.17|; 
$noresave{$key} = "$nosave";

$key = q/eq:cinpot/;
$external_latex_labels{$key} = q|1.4|; 
$noresave{$key} = "$nosave";

$key = q/eq:pot/;
$external_latex_labels{$key} = q|3.11|; 
$noresave{$key} = "$nosave";

1;

