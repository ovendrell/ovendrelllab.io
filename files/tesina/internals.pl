# LaTeX2HTML 2002-2-1 (1.70)
# Associate internals original text with physical files.


$key = q/eq:cinpot/;
$ref_files{$key} = "$dir".q|node14.html|; 
$noresave{$key} = "$nosave";

$key = q/eq:wavefunc/;
$ref_files{$key} = "$dir".q|node14.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_becke-5648/;
$ref_files{$key} = "$dir".q|node32.html|; 
$noresave{$key} = "$nosave";

$key = q/tab:ele/;
$ref_files{$key} = "$dir".q|node26.html|; 
$noresave{$key} = "$nosave";

$key = q/eq:cart/;
$ref_files{$key} = "$dir".q|node14.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_paz-8114/;
$ref_files{$key} = "$dir".q|node32.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_light-1400/;
$ref_files{$key} = "$dir".q|node32.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_Tunnel/;
$ref_files{$key} = "$dir".q|node32.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_salahub-4439/;
$ref_files{$key} = "$dir".q|node32.html|; 
$noresave{$key} = "$nosave";

$key = q/eq:potential/;
$ref_files{$key} = "$dir".q|node14.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_bosch-2072/;
$ref_files{$key} = "$dir".q|node32.html|; 
$noresave{$key} = "$nosave";

$key = q/fig:matH/;
$ref_files{$key} = "$dir".q|node14.html|; 
$noresave{$key} = "$nosave";

$key = q/fig:splitting/;
$ref_files{$key} = "$dir".q|node9.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_sekiya-1589/;
$ref_files{$key} = "$dir".q|node32.html|; 
$noresave{$key} = "$nosave";

$key = q/fig:splittings/;
$ref_files{$key} = "$dir".q|node23.html|; 
$noresave{$key} = "$nosave";

$key = q/eq:bipot1/;
$ref_files{$key} = "$dir".q|node19.html|; 
$noresave{$key} = "$nosave";

$key = q/fig:caspt2/;
$ref_files{$key} = "$dir".q|node28.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_miller-1982/;
$ref_files{$key} = "$dir".q|node32.html|; 
$noresave{$key} = "$nosave";

$key = q/eq:bipot2/;
$ref_files{$key} = "$dir".q|node19.html|; 
$noresave{$key} = "$nosave";

$key = q/eq:sigma/;
$ref_files{$key} = "$dir".q|node14.html|; 
$noresave{$key} = "$nosave";

$key = q/eq:bipot3/;
$ref_files{$key} = "$dir".q|node19.html|; 
$noresave{$key} = "$nosave";

$key = q/eq:bipot4/;
$ref_files{$key} = "$dir".q|node19.html|; 
$noresave{$key} = "$nosave";

$key = q/eq:bipot5/;
$ref_files{$key} = "$dir".q|node19.html|; 
$noresave{$key} = "$nosave";

$key = q/eq:bipot6/;
$ref_files{$key} = "$dir".q|node19.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_roos-5483/;
$ref_files{$key} = "$dir".q|node32.html|; 
$noresave{$key} = "$nosave";

$key = q/fig:punts_est/;
$ref_files{$key} = "$dir".q|node18.html|; 
$noresave{$key} = "$nosave";

$key = q/eq:bipot7/;
$ref_files{$key} = "$dir".q|node19.html|; 
$noresave{$key} = "$nosave";

$key = q/cinetic/;
$ref_files{$key} = "$dir".q|node14.html|; 
$noresave{$key} = "$nosave";

$key = q/eq:cor1/;
$ref_files{$key} = "$dir".q|node15.html|; 
$noresave{$key} = "$nosave";

$key = q/eq:cor2/;
$ref_files{$key} = "$dir".q|node15.html|; 
$noresave{$key} = "$nosave";

$key = q/fig:rotlevels/;
$ref_files{$key} = "$dir".q|node23.html|; 
$noresave{$key} = "$nosave";

$key = q/eq:cor3/;
$ref_files{$key} = "$dir".q|node15.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_paz-6275/;
$ref_files{$key} = "$dir".q|node32.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_moller-618/;
$ref_files{$key} = "$dir".q|node32.html|; 
$noresave{$key} = "$nosave";

$key = q/eq:pot/;
$ref_files{$key} = "$dir".q|node20.html|; 
$noresave{$key} = "$nosave";

$key = q/eq:cor4/;
$ref_files{$key} = "$dir".q|node15.html|; 
$noresave{$key} = "$nosave";

$key = q/eq:ang/;
$ref_files{$key} = "$dir".q|node14.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_PTReactions/;
$ref_files{$key} = "$dir".q|node32.html|; 
$noresave{$key} = "$nosave";

$key = q/fig:metil/;
$ref_files{$key} = "$dir".q|node22.html|; 
$noresave{$key} = "$nosave";

$key = q/fig:xy/;
$ref_files{$key} = "$dir".q|node19.html|; 
$noresave{$key} = "$nosave";

$key = q/fig:xz/;
$ref_files{$key} = "$dir".q|node20.html|; 
$noresave{$key} = "$nosave";

$key = q/eq:dvr_potential/;
$ref_files{$key} = "$dir".q|node14.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_makri-4026/;
$ref_files{$key} = "$dir".q|node32.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_scheiner-5898/;
$ref_files{$key} = "$dir".q|node32.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_rossetti-1546/;
$ref_files{$key} = "$dir".q|node32.html|; 
$noresave{$key} = "$nosave";

$key = q/mat_cinetic/;
$ref_files{$key} = "$dir".q|node14.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_ito-517/;
$ref_files{$key} = "$dir".q|node32.html|; 
$noresave{$key} = "$nosave";

$key = q/fig:fun1_6/;
$ref_files{$key} = "$dir".q|node24.html|; 
$noresave{$key} = "$nosave";

$key = q/fig:fun7_12/;
$ref_files{$key} = "$dir".q|node24.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_sekiya-49/;
$ref_files{$key} = "$dir".q|node32.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_parr-785/;
$ref_files{$key} = "$dir".q|node32.html|; 
$noresave{$key} = "$nosave";

$key = q/fig:tropolona/;
$ref_files{$key} = "$dir".q|node8.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_baughcum-6296/;
$ref_files{$key} = "$dir".q|node32.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_baughcum-2260/;
$ref_files{$key} = "$dir".q|node32.html|; 
$noresave{$key} = "$nosave";

$key = q/tab:comp/;
$ref_files{$key} = "$dir".q|node27.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_sekiya-377/;
$ref_files{$key} = "$dir".q|node32.html|; 
$noresave{$key} = "$nosave";

$key = q/eq:schroringer/;
$ref_files{$key} = "$dir".q|node14.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_makri-1451/;
$ref_files{$key} = "$dir".q|node32.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_bosch-5685/;
$ref_files{$key} = "$dir".q|node32.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_molpro/;
$ref_files{$key} = "$dir".q|node32.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_stratmann-8218/;
$ref_files{$key} = "$dir".q|node32.html|; 
$noresave{$key} = "$nosave";

$key = q/eq:schroringer2/;
$ref_files{$key} = "$dir".q|node14.html|; 
$noresave{$key} = "$nosave";

$key = q/fig:fun1_6D/;
$ref_files{$key} = "$dir".q|node24.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_sekiya-3961/;
$ref_files{$key} = "$dir".q|node32.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_roos-1218/;
$ref_files{$key} = "$dir".q|node32.html|; 
$noresave{$key} = "$nosave";

$key = q/eq:rot2/;
$ref_files{$key} = "$dir".q|node20.html|; 
$noresave{$key} = "$nosave";

$key = q/eq:rot3/;
$ref_files{$key} = "$dir".q|node20.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_werner-5546/;
$ref_files{$key} = "$dir".q|node32.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_lapack/;
$ref_files{$key} = "$dir".q|node32.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_paz-353/;
$ref_files{$key} = "$dir".q|node32.html|; 
$noresave{$key} = "$nosave";

$key = q/eq:rot/;
$ref_files{$key} = "$dir".q|node20.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_g98/;
$ref_files{$key} = "$dir".q|node32.html|; 
$noresave{$key} = "$nosave";

$key = q/fig:fun7_12D/;
$ref_files{$key} = "$dir".q|node24.html|; 
$noresave{$key} = "$nosave";

$key = q/eq:ordre/;
$ref_files{$key} = "$dir".q|node9.html|; 
$noresave{$key} = "$nosave";

$key = q/fig:mtropolona/;
$ref_files{$key} = "$dir".q|node12.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_sekiya-4802/;
$ref_files{$key} = "$dir".q|node32.html|; 
$noresave{$key} = "$nosave";

$key = q/eq:dvr_cinetic/;
$ref_files{$key} = "$dir".q|node14.html|; 
$noresave{$key} = "$nosave";

$key = q/tab:spl/;
$ref_files{$key} = "$dir".q|node23.html|; 
$noresave{$key} = "$nosave";

1;

