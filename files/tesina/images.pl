# LaTeX2HTML 2002-2-1 (1.71)
# Associate images original text with physical files.


$key = q/7.86;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="35" HEIGHT="17" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img145.png"
 ALT="$7.86$">|; 

$key = q/5.75;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="35" HEIGHT="17" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img151.png"
 ALT="$5.75$">|; 

$key = q/{displaymath}c=frac{8cdot((DeltaE_{LRP})-(DeltaE_{MEP}))}{Deltaycdot(Deltax)^2}{displaymath};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="398" HEIGHT="47" BORDER="0"
 SRC="|."$dir".q|img93.png"
 ALT="\begin{displaymath}
c=\frac{8\cdot ((\Delta E_{LRP})-(\Delta E_{MEP}))} {\Delta y \cdot(\Delta x)^2}
\end{displaymath}">|; 

$key = q/includegraphics{caspt2};LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="382" HEIGHT="289" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img158.png"
 ALT="\includegraphics{caspt2}">|; 

$key = q/0.105;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="44" HEIGHT="17" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img50.png"
 ALT="$0.105$">|; 

$key = q/includegraphics{xz};LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="576" HEIGHT="403" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img114.png"
 ALT="\includegraphics{xz}">|; 

$key = q/0.00;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="35" HEIGHT="17" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img135.png"
 ALT="$0.00$">|; 

$key = q/{mathbb{H}{;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="18" HEIGHT="17" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img28.png"
 ALT="$\mathbb{H}$">|; 

$key = q/DeltaE_{LRP};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="62" HEIGHT="32" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img88.png"
 ALT="$\Delta E_{LRP}$">|; 

$key = q/E_0;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="25" HEIGHT="32" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img74.png"
 ALT="$E_0$">|; 

$key = q/{displaymath}V(Theta)=frac{V_3}{2}(1-cos3Theta)+frac{V_6}{2}(1-cos6Theta){displaymath};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="432" HEIGHT="43" BORDER="0"
 SRC="|."$dir".q|img97.png"
 ALT="\begin{displaymath}
V(\Theta)=\frac{V_3}{2}(1-cos3\Theta)+\frac{V_6}{2}(1-cos6\Theta)
\end{displaymath}">|; 

$key = q/{displaymath}widehat{H}Phi=EPhi{displaymath};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="318" HEIGHT="31" BORDER="0"
 SRC="|."$dir".q|img26.png"
 ALT="\begin{displaymath}
\widehat{H}\Phi = E \Phi
\end{displaymath}">|; 

$key = q/y;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="14" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img8.png"
 ALT="$y$">|; 

$key = q/{displaymath}V_{ii'jj'kk'}=delta_{ii'}delta_{jj'}delta_{kk'}V_{ijk}{displaymath};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="372" HEIGHT="36" BORDER="0"
 SRC="|."$dir".q|img40.png"
 ALT="\begin{displaymath}
V_{ii'jj'kk'}=\delta_{ii'} \delta_{jj'} \delta_{kk'} V_{ijk}
\end{displaymath}">|; 

$key = q/0;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="13" HEIGHT="17" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img106.png"
 ALT="$0$">|; 

$key = q/{displaymath}T_{kk'}=frac{hbar^2}{2m}(-1)^{k-k'}left{array{{ll}frac{N(N+1)}{3}&k{2sin^2[pi(k-k')slash(2N+1)]}&kneqk'array{right.{displaymath};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="465" HEIGHT="88" BORDER="0"
 SRC="|."$dir".q|img45.png"
 ALT="\begin{displaymath}
T_{kk'}=\frac{\hbar^2}{2m}(-1)^{k-k'}
\left \{
\begin{array}...
...{2\sin^2[\pi(k-k')/(2N+1)]} &amp; k\neq k' \\\\
\end{array}\right .
\end{displaymath}">|; 

$key = q/_3;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="12" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img125.png"
 ALT="$_3$">|; 

$key = q/includegraphics{xy};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="576" HEIGHT="403" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img82.png"
 ALT="\includegraphics{xy}">|; 

$key = q/0.49;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="35" HEIGHT="17" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img152.png"
 ALT="$0.49$">|; 

$key = q/{displaymath}a=frac{8cdot(DeltaE_{MEP})}{(Deltax)^2}{displaymath};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="346" HEIGHT="47" BORDER="0"
 SRC="|."$dir".q|img91.png"
 ALT="\begin{displaymath}
a=\frac{8\cdot (\Delta E_{MEP})} {(\Delta x)^2}
\end{displaymath}">|; 

$key = q/3.17;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="35" HEIGHT="17" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img138.png"
 ALT="$3.17$">|; 

$key = q/60^0;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="29" HEIGHT="20" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img9.png"
 ALT="$60^0$">|; 

$key = q/Theta_i;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="23" HEIGHT="32" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img63.png"
 ALT="$\Theta_i$">|; 

$key = q/12.67;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="44" HEIGHT="17" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img148.png"
 ALT="$12.67$">|; 

$key = q/y_i;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="18" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img62.png"
 ALT="$y_i$">|; 

$key = q/0.93;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="35" HEIGHT="17" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img146.png"
 ALT="$0.93$">|; 

$key = q/b;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="12" HEIGHT="17" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img84.png"
 ALT="$b$">|; 

$key = q/17.23;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="44" HEIGHT="17" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img136.png"
 ALT="$17.23$">|; 

$key = q/includegraphics{mtropolona};LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="407" HEIGHT="216" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img25.png"
 ALT="\includegraphics{mtropolona}">|; 

$key = q/_2;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="12" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img5.png"
 ALT="$_2$">|; 

$key = q/-1.73;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="49" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img150.png"
 ALT="$-1.73$">|; 

$key = q/includegraphics{punts_est};LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="383" HEIGHT="323" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img75.png"
 ALT="\includegraphics{punts_est}">|; 

$key = q/3.14times10^{-2};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="92" HEIGHT="38" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img123.png"
 ALT="$3.14 \times 10^{-2}$">|; 

$key = q/1.31;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="35" HEIGHT="17" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img147.png"
 ALT="$1.31$">|; 

$key = q/sigma_i;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="20" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img64.png"
 ALT="$\sigma_i$">|; 

$key = q/x_i;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="20" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img61.png"
 ALT="$x_i$">|; 

$key = q/V_3;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="22" HEIGHT="32" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img98.png"
 ALT="$V_3$">|; 

$key = q/Deltay;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="28" HEIGHT="32" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img90.png"
 ALT="$\Delta y$">|; 

$key = q/Delta;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="19" HEIGHT="17" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img1.png"
 ALT="$\Delta $">|; 

$key = q/omega^2;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="23" HEIGHT="20" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img86.png"
 ALT="$\omega ^2$">|; 

$key = q/{displaymath}T_{ij}=langlePhi_i|widehat{T}|Phi_jrangle{displaymath};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="337" HEIGHT="36" BORDER="0"
 SRC="|."$dir".q|img32.png"
 ALT="\begin{displaymath}
T_{ij}=\langle \Phi_i \vert \widehat{T} \vert \Phi_j \rangle
\end{displaymath}">|; 

$key = q/piRightarrowpi^*;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="60" HEIGHT="17" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img16.png"
 ALT="$\pi \Rightarrow \pi ^*$">|; 

$key = q/4.91;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="35" HEIGHT="17" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img157.png"
 ALT="$4.91$">|; 

$key = q/9.53;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="35" HEIGHT="17" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img153.png"
 ALT="$9.53$">|; 

$key = q/7600times7600;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="95" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img47.png"
 ALT="$7600 \times 7600$">|; 

$key = q/DeltaE_{MEP};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="68" HEIGHT="32" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img87.png"
 ALT="$\Delta E_{MEP}$">|; 

$key = q/i;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="10" HEIGHT="17" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img36.png"
 ALT="$i$">|; 

$key = q/(+);MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="31" HEIGHT="35" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img14.png"
 ALT="$(+)$">|; 

$key = q/S_{1};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="23" HEIGHT="32" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img11.png"
 ALT="$S_{1}$">|; 

$key = q/displaystyle-Big[frac{V_3}{2}(1+cos3{Theta})+frac{V_6}{2}(1-cos6{Theta})Big]Big(frac{x-x_0}{2x_0}Big);MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="356" HEIGHT="56" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img104.png"
 ALT="$\displaystyle -\Big[\frac{V_3}{2}(1+\cos 3{\Theta})+
\frac{V_6}{2}(1-\cos 6{\Theta})\Big]\Big(\frac{x-x_0}{2x_0}\Big)$">|; 

$key = q/{mathbb{E}{;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="16" HEIGHT="17" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img29.png"
 ALT="$\mathbb{E}$">|; 

$key = q/Delta_{1}-Delta_{0};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="70" HEIGHT="32" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img20.png"
 ALT="$\Delta_{1}-\Delta_{0}$">|; 

$key = q/{displaymath}{mathbb{T}{={mathbb{T}{_i+{mathbb{T}{_j+{mathbb{T}{_k{displaymath};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="345" HEIGHT="36" BORDER="0"
 SRC="|."$dir".q|img41.png"
 ALT="\begin{displaymath}
\mathbb{T}=\mathbb{T}_i + \mathbb{T}_j + \mathbb{T}_k
\end{displaymath}">|; 

$key = q/E=Bm^2;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="78" HEIGHT="20" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img116.png"
 ALT="$E=Bm^2$">|; 

$key = q/1.51;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="35" HEIGHT="17" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img137.png"
 ALT="$1.51$">|; 

$key = q/{displaymath}sigma_{i}^{DVR}(x,y,Theta)=frac{sinleft(frac{pileft(x-x_iright)}{Dea_iright)}{sinleft(frac{Theta-Theta_i}{2}right)}{displaymath};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="556" HEIGHT="74" BORDER="0"
 SRC="|."$dir".q|img59.png"
 ALT="\begin{displaymath}
\sigma_{i}^{DVR}(x,y,\Theta)=\frac{\sin\left(\frac{\pi\left(...
...-\Theta_i\right)}
{\sin\left(\frac{\Theta-\Theta_i}{2}\right)}
\end{displaymath}">|; 

$key = q/1;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="13" HEIGHT="17" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img108.png"
 ALT="$1$">|; 

$key = q/sigma_{i};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="20" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img57.png"
 ALT="$\sigma_{i}$">|; 

$key = q/Delta_1;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="26" HEIGHT="32" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img22.png"
 ALT="$\Delta_1$">|; 

$key = q/includegraphics[scale=0.7]{HCH3};LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="350" HEIGHT="246" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img132.png"
 ALT="\includegraphics[scale=0.7]{HCH3}">|; 

$key = q/E_{CIS}^{ex};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="44" HEIGHT="32" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img66.png"
 ALT="$E_{CIS}^{ex}$">|; 

$key = q/{displaymath}T_{ii'jj'kk'}=T_{ii'}delta_{jj'}delta_{kk'}+T_{jj'}delta_{ii'}delta_{kk'}+T_{kk'}delta_{ii'}delta_{jj'}{displaymath};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="457" HEIGHT="36" BORDER="0"
 SRC="|."$dir".q|img42.png"
 ALT="\begin{displaymath}
T_{ii'jj'kk'} = T_{ii'}\delta_{jj'} \delta_{kk'} + T_{jj'}\delta_{ii'} \delta_{kk'} + T_{kk'}\delta_{ii'} \delta_{jj'}
\end{displaymath}">|; 

$key = q/-x_i;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="33" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img81.png"
 ALT="$-x_i$">|; 

$key = q/includegraphics[scale=0.7]{HCD3};LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="350" HEIGHT="246" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img134.png"
 ALT="\includegraphics[scale=0.7]{HCD3}">|; 

$key = q/_{ij};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="16" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img73.png"
 ALT="$_{ij}$">|; 

$key = q/S_0;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="23" HEIGHT="32" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img2.png"
 ALT="$S_0$">|; 

$key = q/Delta_1-Delta_0;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="70" HEIGHT="32" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img4.png"
 ALT="$\Delta _1-\Delta _0$">|; 

$key = q/E_b-E_a;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="66" HEIGHT="32" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img96.png"
 ALT="$E_b - E_a$">|; 

$key = q/{displaymath}{mathbb{H}{{mathbb{C}{={mathbb{E}{{mathbb{C}{{displaymath};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="316" HEIGHT="31" BORDER="0"
 SRC="|."$dir".q|img27.png"
 ALT="\begin{displaymath}
\mathbb{H}\mathbb{C}=\mathbb{E}\mathbb{C}
\end{displaymath}">|; 

$key = q/Delta_0;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="26" HEIGHT="32" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img21.png"
 ALT="$\Delta_0$">|; 

$key = q/30^0;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="29" HEIGHT="20" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img102.png"
 ALT="$30^0$">|; 

$key = q/c;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="12" HEIGHT="17" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img85.png"
 ALT="$c$">|; 

$key = q/m;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="20" HEIGHT="17" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img117.png"
 ALT="$m$">|; 

$key = q/{displaymath}E_{CIS}^{S_1}=E_{HF}^{S_0}+E_{CIS}^{ex}{displaymath};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="359" HEIGHT="36" BORDER="0"
 SRC="|."$dir".q|img65.png"
 ALT="\begin{displaymath}
E_{CIS}^{S_1}=E_{HF}^{S_0}+E_{CIS}^{ex}
\end{displaymath}">|; 

$key = q/cdot;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="9" HEIGHT="17" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img51.png"
 ALT="$\cdot$">|; 

$key = q/displaystyleV({Theta};x)=Big[frac{V_3}{2}(1-cos3{Theta})+frac{V_6}{2}(1-cos6{Theta})Big]Big(frac{x+x_0}{2x_0}Big);MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="425" HEIGHT="56" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img103.png"
 ALT="$\displaystyle V({\Theta};x)=\Big[\frac{V_3}{2}(1-\cos 3{\Theta})+
\frac{V_6}{2}(1-\cos 6{\Theta})\Big]\Big(\frac{x+x_0}{2x_0}\Big)$">|; 

$key = q/includegraphics{matH};LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="407" HEIGHT="271" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img43.png"
 ALT="\includegraphics{matH}">|; 

$key = q/2.85times10^{-4};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="92" HEIGHT="38" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img121.png"
 ALT="$2.85 \times 10^{-4}$">|; 

$key = q/{displaymath}g(x)=cx^2{displaymath};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="319" HEIGHT="35" BORDER="0"
 SRC="|."$dir".q|img78.png"
 ALT="\begin{displaymath}
g(x)=cx^2
\end{displaymath}">|; 

$key = q/x=-x_0;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="69" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img107.png"
 ALT="$x=-x_0$">|; 

$key = q/^1pipi^*;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="41" HEIGHT="20" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img19.png"
 ALT="$^1 \pi \pi^*$">|; 

$key = q/m=0,1,2,...n;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="117" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img118.png"
 ALT="$m=0,1,2,...n$">|; 

$key = q/{displaymath}Psi_{j}^{DVR}(x,y,Theta)=sum_{i=1}^{N_p}c_{ij}sigma_{i}^{DVR}(x,y,Theta){displaymath};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="420" HEIGHT="63" BORDER="0"
 SRC="|."$dir".q|img55.png"
 ALT="\begin{displaymath}
\Psi_{j}^{DVR}(x,y,\Theta)=\sum_{i=1}^{N_p}c_{ij}\sigma_{i}^{DVR}(x,y,\Theta)
\end{displaymath}">|; 

$key = q/{displaymath}E_{cor}^{S_1}=E_{CIS}^{S_1}-E_{HF}^{S_0}+E_{DFT}^{S_0}{displaymath};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="389" HEIGHT="36" BORDER="0"
 SRC="|."$dir".q|img67.png"
 ALT="\begin{displaymath}
E_{cor}^{S_1}=E_{CIS}^{S_1}-E_{HF}^{S_0}+E_{DFT}^{S_0}
\end{displaymath}">|; 

$key = q/V(30^0);MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="57" HEIGHT="38" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img101.png"
 ALT="$V(30^0)$">|; 

$key = q/1.70;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="35" HEIGHT="17" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img156.png"
 ALT="$1.70$">|; 

$key = q/V_{ts};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="26" HEIGHT="32" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img122.png"
 ALT="$V_{ts}$">|; 

$key = q/includegraphics{metil};LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="323" HEIGHT="301" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img115.png"
 ALT="\includegraphics{metil}">|; 

$key = q/-6.12;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="49" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img155.png"
 ALT="$-6.12$">|; 

$key = q/displaystyle+Big[frac{V_3}{2}-frac{V_{ts}-V_6}{2}(1-cos6{Theta})Big]Big(frac{x^2-x_{0}^2}{x_{0}^2}Big);MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="315" HEIGHT="61" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img105.png"
 ALT="$\displaystyle +\Big[\frac{V_3}{2}-\frac{V_{ts}-V_6}{2}
(1-\cos 6{\Theta})\Big]\Big(\frac{x^2-x_{0}^2}{x_{0}^2}\Big)$">|; 

$key = q/1.41;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="35" HEIGHT="17" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img149.png"
 ALT="$1.41$">|; 

$key = q/{mathbb{C}{;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="17" HEIGHT="17" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img30.png"
 ALT="$\mathbb{C}$">|; 

$key = q/V_{tot};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="32" HEIGHT="32" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img7.png"
 ALT="$V_{tot}$">|; 

$key = q/V_{0}(x,y);MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="63" HEIGHT="35" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img79.png"
 ALT="$V_{0}(x,y)$">|; 

$key = q/k;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="14" HEIGHT="17" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img38.png"
 ALT="$k$">|; 

$key = q/0.27;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="35" HEIGHT="17" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img154.png"
 ALT="$0.27$">|; 

$key = q/3A;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="26" HEIGHT="17" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img131.png"
 ALT="$3A$">|; 

$key = q/{displaymath}^1pipi^*<S_0<^3pipi^*<^1npi^*<^3npi^*{displaymath};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="406" HEIGHT="34" BORDER="0"
 SRC="|."$dir".q|img18.png"
 ALT="\begin{displaymath}
^1 \pi \pi^* &lt; S_0 &lt; ^3 \pi \pi^* &lt; ^1n\pi^* &lt; ^3n\pi^*
\end{displaymath}">|; 

$key = q/N_p;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="26" HEIGHT="32" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img58.png"
 ALT="$N_p$">|; 

$key = q/0.092;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="44" HEIGHT="17" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img53.png"
 ALT="$0.092$">|; 

$key = q/{displaymath}langlechi_j^b|widehat{H}|chi_i^arangle=E_0delta_{ij}delta_{ab}+F_{aj}delta_{ab}+langleaj|ibrangle-langleaj|birangle{displaymath};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="494" HEIGHT="38" BORDER="0"
 SRC="|."$dir".q|img71.png"
 ALT="\begin{displaymath}
\langle \chi_j ^b \vert \widehat{H} \vert \chi_i ^a \rangle ...
...{ab} + \langle aj\vert ib \rangle - \langle aj\vert bi \rangle
\end{displaymath}">|; 

$key = q/(-);MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="31" HEIGHT="35" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img13.png"
 ALT="$(-)$">|; 

$key = q/57.76times10^6;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="90" HEIGHT="38" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img48.png"
 ALT="$57.76 \times 10^6$">|; 

$key = q/c_{ij};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="24" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img56.png"
 ALT="$c_{ij}$">|; 

$key = q/{displaymath}omega^2=frac{2cdot((DeltaE_{LRP})-(DeltaE_{MEP}))}{(Deltay)^2}{displaymath};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="403" HEIGHT="47" BORDER="0"
 SRC="|."$dir".q|img94.png"
 ALT="\begin{displaymath}
\omega ^2=\frac{2\cdot ((\Delta E_{LRP})-(\Delta E_{MEP}))} {(\Delta y)^2}
\end{displaymath}">|; 

$key = q/2N+1;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="59" HEIGHT="32" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img46.png"
 ALT="$2N+1$">|; 

$key = q/Deltas;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="27" HEIGHT="17" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img60.png"
 ALT="$\Delta s$">|; 

$key = q/22.99;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="44" HEIGHT="17" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img143.png"
 ALT="$22.99$">|; 

$key = q/5.63;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="35" HEIGHT="17" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img140.png"
 ALT="$5.63$">|; 

$key = q/0.67;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="35" HEIGHT="17" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img139.png"
 ALT="$0.67$">|; 

$key = q/Deltax;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="29" HEIGHT="17" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img89.png"
 ALT="$\Delta x$">|; 

$key = q/^{-1};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="22" HEIGHT="20" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img24.png"
 ALT="$^{-1}$">|; 

$key = q/includegraphics{annex};LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="534" HEIGHT="543" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img160.png"
 ALT="\includegraphics{annex}">|; 

$key = q/{displaymath}{mathbb{H}{={mathbb{T}{+{mathbb{V}{{displaymath};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="321" HEIGHT="32" BORDER="0"
 SRC="|."$dir".q|img31.png"
 ALT="\begin{displaymath}
\mathbb{H}=\mathbb{T}+\mathbb{V}
\end{displaymath}">|; 

$key = q/x=x_0;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="55" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img109.png"
 ALT="$x=x_0$">|; 

$key = q/{displaymath}V_{ij}=langlePhi_i|widehat{V}|Phi_jrangle{displaymath};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="338" HEIGHT="36" BORDER="0"
 SRC="|."$dir".q|img33.png"
 ALT="\begin{displaymath}
V_{ij}=\langle \Phi_i \vert \widehat{V} \vert \Phi_j \rangle
\end{displaymath}">|; 

$key = q/20times20times19;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="99" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img49.png"
 ALT="$20 \times 20 \times 19$">|; 

$key = q/a;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="14" HEIGHT="17" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img83.png"
 ALT="$a$">|; 

$key = q/120^0;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="38" HEIGHT="20" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img10.png"
 ALT="$120^0$">|; 

$key = q/{mathbb{V}{;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="17" HEIGHT="17" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img39.png"
 ALT="$\mathbb{V}$">|; 

$key = q/Theta;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="18" HEIGHT="17" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img35.png"
 ALT="$\Theta$">|; 

$key = q/{displaymath}b=frac{16cdot(DeltaE_{MEP})}{(Deltax)^4}{displaymath};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="348" HEIGHT="47" BORDER="0"
 SRC="|."$dir".q|img92.png"
 ALT="\begin{displaymath}
b=\frac{16\cdot (\Delta E_{MEP})} {(\Delta x)^4}
\end{displaymath}">|; 

$key = q/3timesDeltaE_{MEP};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="98" HEIGHT="32" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img124.png"
 ALT="$3 \times \Delta E_{MEP}$">|; 

$key = q/x;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="14" HEIGHT="17" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img34.png"
 ALT="$x$">|; 

$key = q/includegraphics{rotlevels};LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="382" HEIGHT="481" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img119.png"
 ALT="\includegraphics{rotlevels}">|; 

$key = q/160000times160000;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="130" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img54.png"
 ALT="$160000 \times 160000$">|; 

$key = q/V({Theta};0)=frac{V_{ts}}{2}(1-cos6Theta);MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="200" HEIGHT="40" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img112.png"
 ALT="$V({\Theta};0)=\frac{V_{ts}}{2}(1-\cos 6\Theta)$">|; 

$key = q/V_6;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="22" HEIGHT="32" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img99.png"
 ALT="$V_6$">|; 

$key = q/approx;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="18" HEIGHT="17" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img128.png"
 ALT="$\approx$">|; 

$key = q/2.26;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="35" HEIGHT="17" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img23.png"
 ALT="$2.26$">|; 

$key = q/E_b-E_c;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="65" HEIGHT="32" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img95.png"
 ALT="$E_b - E_c$">|; 

$key = q/{displaymath}V_6=V(30^0)-frac{V_3}{2}{displaymath};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="346" HEIGHT="43" BORDER="0"
 SRC="|."$dir".q|img100.png"
 ALT="\begin{displaymath}
V_6 = V(30^0) - \frac{V_3}{2}
\end{displaymath}">|; 

$key = q/{displaymath}T_{ii'}=frac{hbar^2}{2mDeltax^2}(-1)^{i-i'}left{array{{ll}pi^2slashi'phantom{.}frac{2}{(i-i')^2}&ineqi'array{right.{displaymath};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="422" HEIGHT="86" BORDER="0"
 SRC="|."$dir".q|img44.png"
 ALT="\begin{displaymath}
T_{ii'}=\frac{\hbar^2}{2m\Delta x^2}(-1)^{i-i'}
\left \{
\be...
...om{.}\\\\
\frac{2}{(i-i')^2} &amp; i\neq i' \\\\
\end{array}\right .
\end{displaymath}">|; 

$key = q/{displaymath}V(x,y)=V_{0}(x)+frac{1}{2}m{omega}^2left(y-frac{g(x)}{m{omega}^2}right)^2{displaymath};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="424" HEIGHT="52" BORDER="0"
 SRC="|."$dir".q|img76.png"
 ALT="\begin{displaymath}
V(x,y)=V_{0}(x)+\frac{1}{2}m{\omega}^2\left(y-\frac{g(x)}{m{\omega}^2}\right)^2
\end{displaymath}">|; 

$key = q/^{a)};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="18" HEIGHT="21" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img126.png"
 ALT="$^{a)}$">|; 

$key = q/pi;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="15" HEIGHT="17" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img17.png"
 ALT="$\pi$">|; 

$key = q/0.65;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="35" HEIGHT="17" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img141.png"
 ALT="$0.65$">|; 

$key = q/includegraphics[scale=0.7]{DCH3};LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="350" HEIGHT="246" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img133.png"
 ALT="\includegraphics[scale=0.7]{DCH3}">|; 

$key = q/2E;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="27" HEIGHT="17" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img130.png"
 ALT="$2E$">|; 

$key = q/{displaymath}E_{cor}^{S_1}=E_{DFT}^{S_0}+E_{CIS}^{ex}{displaymath};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="360" HEIGHT="36" BORDER="0"
 SRC="|."$dir".q|img68.png"
 ALT="\begin{displaymath}
E_{cor}^{S_1}=E_{DFT}^{S_0}+E_{CIS}^{ex}
\end{displaymath}">|; 

$key = q/S_1;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="23" HEIGHT="32" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img3.png"
 ALT="$S_1$">|; 

$key = q/chi_i^a;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="23" HEIGHT="32" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img72.png"
 ALT="$\chi_i^a$">|; 

$key = q/1E;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="27" HEIGHT="17" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img129.png"
 ALT="$1E$">|; 

$key = q/5.07;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="35" HEIGHT="17" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img142.png"
 ALT="$5.07$">|; 

$key = q/V_3=0.6;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="68" HEIGHT="32" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img120.png"
 ALT="$V_3 = 0.6$">|; 

$key = q/j;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="13" HEIGHT="32" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img37.png"
 ALT="$j$">|; 

$key = q/{displaymath}V_{tot}=V(x,y)+V(x;{Theta}){displaymath};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="370" HEIGHT="35" BORDER="0"
 SRC="|."$dir".q|img113.png"
 ALT="\begin{displaymath}
V_{tot}=V(x,y)+V(x;{\Theta})
\end{displaymath}">|; 

$key = q/{displaymath}V_{0}(x)=-ax^2+bx^4{displaymath};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="355" HEIGHT="35" BORDER="0"
 SRC="|."$dir".q|img77.png"
 ALT="\begin{displaymath}
V_{0}(x)=-ax^2+bx^4
\end{displaymath}">|; 

$key = q/V(x,y);MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="59" HEIGHT="35" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img6.png"
 ALT="$V(x,y)$">|; 

$key = q/x=0;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="46" HEIGHT="17" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img111.png"
 ALT="$x=0$">|; 

$key = q/g(x);MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="37" HEIGHT="35" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img80.png"
 ALT="$g(x)$">|; 

$key = q/0A;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="26" HEIGHT="17" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img127.png"
 ALT="$0A$">|; 

$key = q/langlechi_j^b|widehat{H}|chi_i^arangle;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="80" HEIGHT="43" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img70.png"
 ALT="$\langle \chi_j ^b \vert \widehat{H} \vert \chi_i ^a \rangle$">|; 

$key = q/(cm^{-1});MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="59" HEIGHT="38" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img12.png"
 ALT="$(cm^{-1})$">|; 

$key = q/x_0;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="22" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img110.png"
 ALT="$x_0$">|; 

$key = q/includegraphics{tropo};LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="534" HEIGHT="427" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img15.png"
 ALT="\includegraphics{tropo}">|; 

$key = q/langlechi_0|widehat{H}|chi_i^arangle;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="81" HEIGHT="43" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img69.png"
 ALT="$\langle \chi_0 \vert \widehat{H} \vert \chi_i ^a \rangle$">|; 

$key = q/^0;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="12" HEIGHT="20" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img159.png"
 ALT="$^0$">|; 

$key = q/1.50;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="35" HEIGHT="17" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img144.png"
 ALT="$1.50$">|; 

$key = q/^{1slash2};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="25" HEIGHT="21" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img52.png"
 ALT="$^{1/2}$">|; 

1;

