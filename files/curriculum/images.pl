# LaTeX2HTML 2002-2-1 (1.70)
# Associate images original text with physical files.


$key = q/{revnumerate}{setlength{itemsep}{-0.1cm}{item{{em{Poster:}``Full-DimensionalQuanptember2001,{{bf{O.Vendrell},M.Moreno,J.M.Lluch.{revnumerate};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="542" HEIGHT="572" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img3.png"
 ALT="\begin{revnumerate}
\setlength{\itemsep}{-0.1cm}
\item {\em Poster:}
\lq\lq Full...
..., September 2001,
{\bf O. Vendrell}, M. Moreno, J. M. Lluch.
\end{revnumerate}">|; 

$key = q/bullet;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="13" HEIGHT="17" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img1.png"
 ALT="$ \bullet$">|; 

$key = q/_1;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="12" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img4.png"
 ALT="$ _1$">|; 

$key = q/_0;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="12" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img5.png"
 ALT="$ _0$">|; 

$key = q/{}^1pisigma^*;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="41" HEIGHT="18" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img7.png"
 ALT="$ {}^1\pi\sigma^*$">|; 

$key = q/{}^1pipi^*;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="41" HEIGHT="18" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img6.png"
 ALT="$ {}^1\pi\pi^*$">|; 

$key = q/{revnumerate}{setlength{itemsep}{-0.1cm}{itemO.Vendrell,R.Gelabert,M.Moreno,J.M.ation},Chem.Phys.Lett.,{{bf{334},(2001),112--118{revnumerate};MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="549" HEIGHT="995" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img2.png"
 ALT="\begin{revnumerate}
\setlength{\itemsep}{-0.1cm}
\item O. Vendrell, R. Gelab...
...l cation},
Chem. Phys. Lett., {\bf 334}, (2001), 112 - 118
\end{revnumerate}">|; 

1;

