#!/usr/bin/perl -w
 use strict;

# Given an axis (x,y or z) and an angle of rotation, rotates a
# given set of points in space by the specified ammount by multiplying
# by the corresponding rotation matrix.


# Variables
     my $axis;      # axis of rotation		x,y or z
     my $angle;     # angle of rotation
     my $file;
     my $pi=3.1415927;
 
# Offer help
if( $ARGV[0] =~ /-h|-help/i ) {
	print  <<"EOF";
usage: rotate.pl {-help} | { [-x|-y|-z] [-deg num | -rad num] [-f filename]}
EOF
exit 0;
}


# Process input line parameters
while($_=shift(@ARGV)) {
    if( $_ =~ /-x/i ) {
	    $axis="x";
	    next;
    }
    if( $_ =~ /-y/i ) {
	    $axis="y";
	    next;
    }
    if( $_ =~ /-z/i ) {
	    $axis="z";
	    next;
    }    
    if( $_ =~ /-deg/i ){
	    $angle=shift(@ARGV) * $pi / 180.0 ;
	    next;
    }
    if( $_ =~ /-rad/i ){
	    $angle=shift(@ARGV);
	    next;
    }
    if( $_ =~ /-f/i ){
	    $file=shift(@ARGV);
	    next;
    }
}

# Generate rotation matrix
my $rotmat= &getRotation($axis,$angle);

# Open file for reading data and multiply
open(DAT,$file) || die("Unable to open $file : $!");

my @line;
my @rot;
while(<DAT>){
	@line = split(' ',$_);
        if($#line == 3){
		@rot=&multiply($rotmat,$line[1],$line[2],$line[3]);
	        if($line[0] =~/[a-zA-Z]+/i) {
                        printf "%s  %5.2f  %5.2f  %5.2f\n",$line[0],$rot[0],$rot[1],$rot[2];
                }
                else{
                        printf "%u  %5.2f  %5.2f  %5.2f\n",$line[0],$rot[0],$rot[1],$rot[2];
                }
	}
	else {
		@rot=&multiply($rotmat,$line[0],$line[1],$line[2]);
		printf "%5.2f  %5.2f  %5.2f\n",$rot[0],$rot[1],$rot[2];
	}
}

exit 0;



sub getRotation {

        my $axis= $_[0];
        my $angle=$_[1];
        my $matrix;

        my $sin=sin($angle);
        my $cos=cos($angle);

        if($axis eq "z") {
	        $matrix = [ [ $cos, $sin,  0  ],
                            [-$sin, $cos,  0  ],
                            [   0 ,   0 ,  1  ] ];
        }

        if($axis eq "y") {
                $matrix = [ [ $cos,   0, $sin ],
                            [   0 ,   1,   0  ],
                            [-$sin,   0, $cos ] ];
       }        

       if($axis eq "x") {
                $matrix = [ [   1 ,   0 ,   0 ],
                            [   0 , $cos, $sin],
                            [   0 ,-$sin, $cos] ];
       }

       return $matrix;
}


sub multiply {

        my $m=$_[0];
        my $v0=$_[1];
        my $v1=$_[2];
        my $v2=$_[3];

        my @u;

        $u[0] = $m->[0][0] * $v0 + $m->[0][1] * $v1 + $m->[0][2] * $v2;
        $u[1] = $m->[1][0] * $v0 + $m->[1][1] * $v1 + $m->[1][2] * $v2;
        $u[2] = $m->[2][0] * $v0 + $m->[2][1] * $v1 + $m->[2][2] * $v2;

        return @u;
}




exit 0;

