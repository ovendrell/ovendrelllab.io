#!/usr/bin/perl -w
use strict;

# eq2eps.pl reads a mathematical equation in LaTeX format
# in STDIN and returns it as output in Encapsulated Postscript
# format from STDOUT

# Requires:	LaTeX
#		dvips
#		ps2epsi
# which are ususally present in all linux distributions


# Some help lines
if( @ARGV && $ARGV[0] eq '-h' ){
	print <<'EOF';
Usage: eq2eps.pl [-h] stdin stdout
EOF
exit 0;
}


# Read standard input lines, remove newline character and 
# join them all into one single variable

my @eq;
my $eq;
while(<STDIN>){
	push(@eq,$_);
}
$eq=join('',@eq);

# Change work directory to the temporal directory;
my $olddir=`pwd`;
chdir "/tmp";

# Generate the .tex file
my $file="/tmp/tmp_eq2eps";
open(TEX,">$file.tex") || die("Unable to open .tex file for writing: $!");
print TEX <<'EOF';
\documentclass{article}
\usepackage{amsfonts}
\usepackage{amsmath}
\usepackage{bm}
\begin{document}
\thispagestyle{empty}
\begin{eqnarray*}
EOF
print TEX "$eq";
print TEX <<'EOF';
\end{eqnarray*}
\end{document}
EOF
close TEX;

# Process .tex file using latex
system("latex $file 1>/dev/null 2>/dev/null") ;

# Convert .dvi to .ps
system("dvips -Ppdf $file.dvi -o $file.ps 1>/dev/null 2>/dev/null") ;

# Generate .eps file
system("ps2epsi $file.ps $file.eps") ;

# Pipe .eps file through STDOUT and clean /tmp dir
open(EPS,"$file.eps") ;
while(<EPS>){print $_}
close EPS;
unlink(<$file.*>); 

exit 0;
