#!/usr/bin/perl -w
use strict;


my @columns;

while(@ARGV){
        push @columns, shift @ARGV;
}


while(<STDIN>){
        chomp $_;
        @_=split ' ',$_;
        foreach my $col (@columns){
                print "$_[$col-1]   ";
        }
        print "\n";
}

exit 0;
