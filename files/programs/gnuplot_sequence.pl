#!/usr/bin/perl -w
use strict;

# Takes a file with gnuplot instructions, a file with data segments
# which are separated by two newline characters, and produces a sequence
# of plots, one for each data segment


#my $terminal="postscript eps enhanced color";
my $terminal="gif";
my $filebase="frame";
my  $nframes=500;
my    $count=501;


my $datafile=$ARGV[1];

open(GNUFILE,$ARGV[0]) or die;
my @gnu=<GNUFILE>;
my $gnu=join('',@gnu);
close GNUFILE;

open(GNUPLOT,"| gnuplot") or die("Unable to open a pipe to gnuplot, $!\n");

$gnu=$gnu."set terminal $terminal\n";
print GNUPLOT $gnu;


for(my $i=0; $i<=$nframes; $i=$i+$count) {
        my $ii=sprintf "%04u",$i;
        print GNUPLOT "set title \"Time: $i fs\"\n";
        print GNUPLOT "set output \"$filebase$ii\"\n";
        print GNUPLOT "splot \"$datafile\" index $i\n";
}
        
exit 0;
