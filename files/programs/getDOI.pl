#!/usr/bin/perl -w
use strict;
require LWP::UserAgent;

# ------------------------------------------------------------
# # Purpose
# # ------------------------------------------------------------
# #   Reads bibliographic information in BibTex format
# #   throught STDIN and outputs the same BibTex entries with
# #   the DOI field throught STDOUT, when possible
# #
# #                  Oriol Vendrell Romagosa, UAB, June 2005
# # ------------------------------------------------------------

# HTTP::Request is a class encapsulating HTTP style requests, 
# consisting of a request line, some headers, and a content 
# body. Note that the LWP library uses HTTP style requests even 
# for non-HTTP protocols. Instances of this class are usually 
# passed to the request() method of an LWP::UserAgent object.


ext: while(<STDIN>){
        if($_=~/ARTICLE{/i){
                my @article;
                push(@article,$_);
                $_=<STDIN>;
                until($_=~/}\n/){
                    push(@article,$_);
                    $_=<STDIN>;
                }
                push(@article,"}\n");

                my $year;
                my $journal;
                my $volume;
                my $page;
                foreach my $entry (@article){
                        {
#                               if($entry=~/author = {(.+)},\n/){
#                                       @_=split 'and',$1;
#                                       my $first=$_[0];
#                                       @_=split ' ',$first;
#                                       $author=$_[$#_];
#                                       last;
#                               }
                                if($entry=~/pages = {(.+)},\n/){
                                        $_=$1;
                                        $_=~s/ //;
                                        @_=split '--',$_;
                                        $page=$_[0];
                                        last;
                                }

                                if($entry=~/volume = {(.+)},\n/){
                                        $volume=$1;
                                        last;
                                }

                                if($entry=~/year = {(.+)},\n/){
                                        $year=$1;
                                        last;
                                }

                                if($entry=~/journal = {(.+)},\n/){
                                        $_=$1;
                                        $_=~s/ /\+/g;
                                        $journal=$_;
                                        last;
                                }

                                if($entry=~/doi/){
                                        print @article;
                                        next ext;
                                }
                        }
                }
                my $doi=&getDoi($page,$volume,$year,$journal);
                pop @article;
                if($doi){
                        push(@article,"  doi = {$doi},\n");
                }
                push(@article,"}\n");
                print @article;
                #sleep 1;
                                
        }
        else{
                print $_;
        }
}
                
exit 0;



sub getDoi{
        my $pag=$_[0];
        my $vol=$_[1];
        my $yea=$_[2];
        my $jou=$_[3];

        my $doi;

        my $browser = LWP::UserAgent->new;
        my $url='http://www.crossref.org/guestquery/';
        my $form = "search_type=journal&auth=&issn=&title=$jou&art_title=&volume=$vol&issue=&page=$pag&year=$yea&isbn=&comp_num=&series_title=&multi_hit=on&view_records=Search";
        my $req = HTTP::Request->new(POST => $url);

        $req->content_type('application/x-www-form-urlencoded');
        $req->content($form);

        my $res = $browser->request($req);
        $res = $res->content;

        @_=split "\n", $res;
        foreach my $line (@_) {
#               if($line=~/<a href=(http:\/\/dx\.doi\.org\/.+)> /){
                if($line=~/<a href=http:\/\/dx\.doi\.org\/(.+)> /){
                        $doi=$1;
                        last;
                }
        }
        return $doi;
}


