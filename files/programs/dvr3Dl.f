c
cDiscrete Variable Representation en tres dimensions, emprant la base/punts
csuggerits per Miller a J.Chem.Phys. 96, 1982 (1992).
c
c INPUT:
c xmin,xmax,ymin,ymax,  = interval de treball en l'eix x, idem en l'eix de la y,
c npx,npy,npz             idem en l'eix z i nombre de punts (base DVR) en cada 
c                         direccio 
c
c freq,....              = parametres del potencial 
c
c Valors que es calculen:
c xp=graella de punts DVR x
c yp=graella de punts DVR y
c zp=graella de punts DVR z
c
c
          program dvr3D
          implicit real*8(a-h,o-z)
	  real*8 mcoef(5000,5000)
          dimension scratch(15000),energy(5000)
          dimension h(5000*(5000+1)/2)
c
	  character*1 jobz,uplo
	  integer info
c
          common /dvrdata/ xmin,xmax,ymin,ymax,npx,npy,npz
          common /basis/ xp(5000),yp(5000),zp(5000),ntot
	  common /consts/ pi,zi
c
c el seguent common contindra les dades de la superficie analitica
c
	  common /potenti/ a1,b1,c1,w2,v3,v6,vts
	  c=2.99792458d10
          pi=4.*atan(1.)
          write(6,*) 'pi = ',pi
          read(5,*) xmin,xmax,ymin,ymax,npx,npy,npz
          read(5,*) a1,b1,c1,w2
	  read(5,*) v3,v6,vts
	  read(5,*) zi
c
	  jobz='V'
	  uplo='U'
c
          call distribute
c
          call calcmat(h)
c
          call dspev(jobz,uplo,ntot,h,energy,mcoef,ntot,scratch,info)
          write(6,*) '***'
          write(6,*) 'INFO:',info
          write(6,*) ' '                                             
c
          write(6,*) 'nsx=',npx, 'nsy=',npy,'nsz=',npz
          write(6,6) 'cami x:',xmin,'/',xmax
          write(6,6) 'cami y:',ymin,'/',ymax
          write(6,6) 'cami z:',0,'/',2*pi
	  write(6,1) 'x-Grid Spacing:',(xmax-xmin)/(npx-1)
	  write(6,1) 'y-Grid Spacing:',(ymax-ymin)/(npy-1)
	  write(6,1) 'z-Grid Spacing:',(2*pi)/(npz+1)
c
          call writcoef(ntot,mcoef,energy)
c
	  write(6,2) 'Summary of Energies:'
	  do i=1,ntot
	       write(6,4) i,energy(i)
	  end do
	  write(6,*) ntot
c
c provisionally withdrawn
c
c         write(6,*) 'els splittings son en cm-1 i Hz'
c         do i=ntot,1,-2
c             split=349.75*(energy(i-1)-energy(i))
c             write(6,*) split,'   ',split*2.9979d10
c         end do
          write(6,*) '  ' 
          write(6,*) 'taula PES'
          do i=1,ntot
               write(6,7) xp(i),yp(i),zp(i),pot(xp(i),yp(i),zp(i))
          end do
1         format(a,f10.5)
2	  format(a)
3	  format(5(f10.6,2x))
4         format(i4,2x,f16.8)
5	  format(5x,i4,2x,f10.5)
6         format(a,f5.2,a,f4.2)
7         format(4(2x,f10.6))
          end   


	  subroutine writcoef(ntot,mcoef,energy)
	  implicit real*8(a-h,o-z)
          real*8 mcoef(ntot,ntot),energy(ntot)
	  write(6,2) 'Coefficients:'
	  do j=11,25
             write(6,*) 'nivell',j 
             write(6,*) energy(j),'Kcal/mol'
             write(6,4) (mcoef(i,j),i=1,ntot)
	  end do
2	  format(a)
4	  format(5(1pd18.6,2x))
          return
	  end




          subroutine distribute
          implicit real*8(a-h,o-z)
          common /dvrdata/ xmin,xmax,ymin,ymax,npx,npy,npz
          common /basis/ xp(5000),yp(5000),zp(5000),ntot
	  common /consts/ pi,zi
          dx=(xmax-xmin)/(npx-1)
          dy=(ymax-ymin)/(npy-1)
          dz=(2*pi)/(npz-1)
          ntot=0 
          do i=1,npx
             do j=1,npy
                do k=1,npz
                   ntot=ntot+1
                   xp(ntot)=xmin+(i-1)*dx
                   yp(ntot)=ymin+(j-1)*dy
                   zp(ntot)=0+(k-1)*dz
                end do
             end do
          end do
          return
          end



          subroutine calcmat(h) 
          implicit real*8(a-h,o-z)
          common /dvrdata/ xmin,xmax,ymin,ymax,npx,npy,npz
          common /basis/ xp(5000),yp(5000),zp(5000),ntot
	  common /consts/ pi,zi
          dimension h(ntot*(ntot+1)/2)
          l=1 
c
c	Abans div per np(xyz)-1
c
          dx=(xmax-xmin)/(npx-1)
          dy=(ymax-ymin)/(npy-1)
          dz=(2*pi)/(npz+1)
          do ni=1,ntot
            do nj=1,ni
               i=nint((xp(ni)-xp(1))/dx)
               ip=nint((xp(nj)-xp(1))/dx)
               j=nint((yp(ni)-yp(1))/dy)
               jp=nint((yp(nj)-yp(1))/dy)
               k=nint((zp(ni)-zp(1))/dz)
               kp=nint((zp(nj)-zp(1))/dz)
               if (i.eq.ip) then
                  deltai=1
               else
                  deltai=0
               end if
               if (j.eq.jp) then
                  deltaj=1
               else
                  deltaj=0
               end if
               if (k.eq.kp) then
                  deltak=1
               else
                  deltak=0
               end if
               h(l)=T(i,ip,dx)*deltaj*deltak+T(j,jp,dy)*deltai*deltak
     1              +Tzeta(k,kp)*deltai*deltaj+deltai*deltaj*deltak*
     2               pot(xp(ni),yp(ni),zp(ni))
c		write(6,*) h(l)
               l=l+1
c		write(6,100) 'T(i,ip,dx)',T(i,ip,dx),i,ip
c	        write(6,100) 'T(j,jp,dy)',T(j,jp,dy),j,jp
c	        write(6,100) 'Tzeta(k,kp)',Tzeta(k,kp),k,kp
c	        write(6,100) 'pot',pot(xp(ni),yp(ni),zp(ni))
c	        write(6,*) ' '
c100	        format (a,f18.9,1x,i2,1x,i2)
            end do
          enddo
          return
          end



          function pot(x,y,z)
          implicit real*8(a-h,o-z)
	  common /potenti/ a1,b1,c1,w2,v3,v6,vts 
	  common /consts/ pi,zi
c
c a omplir amb la funcio potencial. x,y i Z en A; pot en kcal/mol
c
	  xo=(a1/(2*b1))**0.5
	  pot=-a1*x**2+b1*x**4+0.5*w2*(y-(c1*x**2)/w2)**2+
     1  (v3/2*(1-cos(3*z))+v6/2*(1-cos(6*z)))*((x+xo)/(2*xo))-
     2  (v3/2*(1+cos(3*z))+v6/2*(1-cos(6*z)))*((x-xo)/(2*xo))+
     3  (v3/2-(vts-v6)/2*(1-cos(6*z)))*(x**2-xo**2)/xo**2
          return
          end


          function T(i,ii,d)
          implicit real*8 (a-h,o-z)
 	  common /consts/ pi,zi
	  hb=1.05457267d-34
	  an=6.0221367d+23
	  fact=hb*hb*an*an*1.d+20*1.d+3/4184.
          a=((pi)**2)/3
          if (i.eq.ii) then
               T=fact*a/(2.*d**2)
               else
               T=fact*(-1)**(i-ii)/(2.*d**2)*(2./(i-ii)**2)
          end if
          return
          end


	  function Tzeta(i,ii)
	  implicit real*8 (a-h,o-z)
	  common /dvrdata/ xmin,xmax,ymin,ymax,npx,npy,npz
	  common /consts/ pi,zi
	  hb=1.05457267d-34
	  an=6.0221367d+23
	  nn=(npz-1)/2
	  fact=hb*hb*an*an*1.d+20*1.d+3/4184.
	  a=nn*(nn+1)/3
	  b=(cos(pi*(i-ii)/(2*nn+1)))/(2*(sin(pi*(i-ii)/(2*nn+1)))**2)
c	  write(6,*) 'num:',(cos(pi*(i-ii)/(2*nn+1))),i,ii
c	  write(6,*) 'den:',(2*(sin(pi*(i-ii)/(2*nn+1)))**2),i,ii
	  if (i.eq.ii) then
		Tzeta=fact*a/(2*zi)
	        else
	        Tzeta=fact*(-1)**(i-ii)/(2*zi)*b
	  endif
	  return
	  end 
               

