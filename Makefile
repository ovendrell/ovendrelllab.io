# Process the web site source using the C preprocessor

base=$(HOME)/Web
source=$(base)/src.public_html
#local=$(base)/public_html
local=$(base)/ovendrell.gitlab.io
#remote=oriol@tc-gate.pci.uni-heidelberg.de:/home/WWW/oriol/
#remote=ovendrel@bastion.desy.de:/afs/desy.de/user/o/ovendrel/www/
#remote=orve@vlifa01.phys.au.dk:/usr/users/orve/public_html/
remote=www.oriolvendrell.science@ssh.strato.de:/mnt/web404/e2/52/59411152/htdocs/

$(shell if [ ! -e $(local) ]; then mkdir $(local); fi)

#gcc=gcc -E -x c -P -C -imacros $(source)/include/macros.h
gcc=gcc -E -w -x c -P -C -traditional

# files to be preprocessed (once copied in the local directory)
html=$(local)/index.src.html \
     $(local)/research_en.src.html \
     $(local)/people.src.html \
     $(local)/oriol.src.html \
     $(local)/research_en_protonwires.src.html \
     $(local)/research_en_protonwires_mov1.src.html \
     $(local)/curriculum_en.src.html \
     $(local)/publications_en.src.html \
     $(local)/teaching_en.src.html \
     $(local)/links.src.html \
     $(local)/programs.src.html \
     $(local)/personal.src.html


process:
	rsync -a $(source)/ $(local)/
	@echo 'preprocessing src.html files'
	@for f in $(html); do                   \
	   $(gcc) $$f > $${f%src.html}html ;    \
	   rm -f $$f ;                          \
	done
	
install: process
	rsync -a --delete -e ssh $(local)/ $(remote)


clean:
	- rm -rf $(local)

