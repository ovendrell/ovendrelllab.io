camera {
location <    -6.83420,   -17.43970,     4.99953>
look_at <     0.00000,     0.00000,     0.00000>
 angle 45
}
sky_sphere {
  pigment {
    gradient y
    color_map {
      [0 color rgb <1, 0, 0>]
      [1 color rgb <0, 0, 1>]
    }
    scale 2
    translate -1
  }
}
light_source { <     9.00000,     9.00000,     9.00000>
 color rgb<1, 1, 1> }
 #declare BSAMBI = 0.2;
 #declare BSDIFF = 0.8;
 #declare BSSPEC = 0.8;
#declare colorA = 
 texture { 
 pigment { rgb<  1.0000  0.0000  0.0000 >}
 finish {ambient BSAMBI diffuse BSDIFF specular BSSPEC}
 }
#declare colorB = 
 texture { 
 pigment { rgb<  1.0000  0.6235  0.0353 >}
 finish {ambient BSAMBI diffuse BSDIFF specular BSSPEC}
 }
#declare colorC = 
 texture { 
 pigment { rgb<  0.0000  1.0000  0.0000 >}
 finish {ambient BSAMBI diffuse BSDIFF specular BSSPEC}
 }
#declare colorD = 
 texture { 
 pigment { rgb<  0.3059  1.0000  0.7333 >}
 finish {ambient BSAMBI diffuse BSDIFF specular BSSPEC}
 }
#declare colorE = 
 texture { 
 pigment { rgb<  0.0000  1.0000  1.0000 >}
 finish {ambient BSAMBI diffuse BSDIFF specular BSSPEC}
 }
#declare colorF = 
 texture { 
 pigment { rgb<  1.0000  0.7490  0.0000 >}
 finish {ambient BSAMBI diffuse BSDIFF specular BSSPEC}
 }
#declare colorG = 
 texture { 
 pigment { rgb<  0.5176  0.7569  0.8392 >}
 finish {ambient BSAMBI diffuse BSDIFF specular BSSPEC}
 }
#declare colorH = 
 texture { 
 pigment { rgb<  0.4510  0.4510  0.4510 >}
 finish {ambient BSAMBI diffuse BSDIFF specular BSSPEC}
 }
#declare colorI = 
 texture { 
 pigment { rgb<  1.0000  0.0000  1.0000 >}
 finish {ambient BSAMBI diffuse BSDIFF specular BSSPEC}
 }
#declare colorJ = 
 texture { 
 pigment { rgb<  0.0627  0.6902  0.0627 >}
 finish {ambient BSAMBI diffuse BSDIFF specular BSSPEC}
 }
#declare colorK = 
 texture { 
 pigment { rgb<  0.9373  0.7922  0.5490 >}
 finish {ambient BSAMBI diffuse BSDIFF specular BSSPEC}
 }
#declare colorL = 
 texture { 
 pigment { rgb<  1.0000  0.4784  0.0000 >}
 finish {ambient BSAMBI diffuse BSDIFF specular BSSPEC}
 }
#declare colorM = 
 texture { 
 pigment { rgb<  0.9020  0.8392  0.3608 >}
 finish {ambient BSAMBI diffuse BSDIFF specular BSSPEC}
 }
#declare colorN = 
 texture { 
 pigment { rgb<  0.7216  0.2196  0.0235 >}
 finish {ambient BSAMBI diffuse BSDIFF specular BSSPEC}
 }
#declare colorO = 
 texture { 
 pigment { rgb<  1.0000  1.0000  1.0000 >}
 finish {ambient BSAMBI diffuse BSDIFF specular BSSPEC}
 }
 # declare molecule = union { 
sphere { 
<    -2.25268,     0.00000,     0.06644>,      0.75589
 texture { colorA }
 }
sphere { 
<    -2.25268,     0.00000,     0.06644>,      0.52859
 texture { colorA }
 }
 cylinder {
<    -2.25268,     0.00000,     0.06644>, <    -2.74283,    -0.23980,    -0.66796>,      0.37
 texture { colorA }
 }
sphere { 
<    -2.25268,     0.00000,     0.06644>,      0.52859
 texture { colorA }
 }
 cylinder {
<    -2.25268,     0.00000,     0.06644>, <    -2.65153,     0.71259,     0.48033>,      0.37
 texture { colorA }
 }
sphere { 
<     2.66061,     0.00000,     0.06644>,      0.75589
 texture { colorA }
 }
sphere { 
<     2.66061,     0.00000,     0.06644>,      0.52859
 texture { colorA }
 }
 cylinder {
<     2.66061,     0.00000,     0.06644>, <     1.61947,     0.00000,    -0.00469>,      0.37
 texture { colorA }
 }
sphere { 
<     2.66061,     0.00000,     0.06644>,      0.52859
 texture { colorA }
 }
 cylinder {
<     2.66061,     0.00000,     0.06644>, <     3.16390,     0.23980,    -0.65901>,      0.37
 texture { colorA }
 }
sphere { 
<     2.66061,     0.00000,     0.06644>,      0.52859
 texture { colorA }
 }
 cylinder {
<     2.66061,     0.00000,     0.06644>, <     3.05194,    -0.71259,     0.48744>,      0.37
 texture { colorA }
 }
sphere { 
<     0.57834,     0.00000,    -0.07582>,      0.37795
 texture { colorO }
 }
sphere { 
<     0.57834,     0.00000,    -0.07582>,      0.52859
 texture { colorO }
 }
 cylinder {
<     0.57834,     0.00000,    -0.07582>, <     1.61947,     0.00000,    -0.00469>,      0.37
 texture { colorO }
 }
sphere { 
<    -3.23298,    -0.47961,    -1.40235>,      0.37795
 texture { colorO }
 }
sphere { 
<    -3.23298,    -0.47961,    -1.40235>,      0.52859
 texture { colorO }
 }
 cylinder {
<    -3.23298,    -0.47961,    -1.40235>, <    -2.74283,    -0.23980,    -0.66796>,      0.37
 texture { colorO }
 }
sphere { 
<     3.66719,     0.47961,    -1.38446>,      0.37795
 texture { colorO }
 }
sphere { 
<     3.66719,     0.47961,    -1.38446>,      0.52859
 texture { colorO }
 }
 cylinder {
<     3.66719,     0.47961,    -1.38446>, <     3.16390,     0.23980,    -0.65901>,      0.37
 texture { colorO }
 }
sphere { 
<    -3.05038,     1.42519,     0.89421>,      0.37795
 texture { colorO }
 }
sphere { 
<    -3.05038,     1.42519,     0.89421>,      0.52859
 texture { colorO }
 }
 cylinder {
<    -3.05038,     1.42519,     0.89421>, <    -2.65153,     0.71259,     0.48033>,      0.37
 texture { colorO }
 }
sphere { 
<     3.44327,    -1.42519,     0.90844>,      0.37795
 texture { colorO }
 }
sphere { 
<     3.44327,    -1.42519,     0.90844>,      0.52859
 texture { colorO }
 }
 cylinder {
<     3.44327,    -1.42519,     0.90844>, <     3.05194,    -0.71259,     0.48744>,      0.37
 texture { colorO }
 }
sphere { 
<     5.22647,     1.22256,    -3.63203>,      0.75589
 texture { colorA }
 }
sphere { 
<     5.22647,     1.22256,    -3.63203>,      0.52859
 texture { colorA }
 }
 cylinder {
<     5.22647,     1.22256,    -3.63203>, <     6.09780,     1.15487,    -3.42727>,      0.37
 texture { colorA }
 }
sphere { 
<     5.22647,     1.22256,    -3.63203>,      0.52859
 texture { colorA }
 }
 cylinder {
<     5.22647,     1.22256,    -3.63203>, <     5.03769,     2.06991,    -3.86026>,      0.37
 texture { colorA }
 }
sphere { 
<     6.96912,     1.08718,    -3.22250>,      0.37795
 texture { colorO }
 }
sphere { 
<     6.96912,     1.08718,    -3.22250>,      0.52859
 texture { colorO }
 }
 cylinder {
<     6.96912,     1.08718,    -3.22250>, <     6.09780,     1.15487,    -3.42727>,      0.37
 texture { colorO }
 }
sphere { 
<     4.84891,     2.91725,    -4.08848>,      0.37795
 texture { colorO }
 }
sphere { 
<     4.84891,     2.91725,    -4.08848>,      0.52859
 texture { colorO }
 }
 cylinder {
<     4.84891,     2.91725,    -4.08848>, <     5.03769,     2.06991,    -3.86026>,      0.37
 texture { colorO }
 }
sphere { 
<     4.57412,    -3.48441,     2.12502>,      0.75589
 texture { colorA }
 }
sphere { 
<     4.57412,    -3.48441,     2.12502>,      0.52859
 texture { colorA }
 }
 cylinder {
<     4.57412,    -3.48441,     2.12502>, <     5.46709,    -3.40585,     2.07861>,      0.37
 texture { colorA }
 }
sphere { 
<     4.57412,    -3.48441,     2.12502>,      0.52859
 texture { colorA }
 }
 cylinder {
<     4.57412,    -3.48441,     2.12502>, <     4.21024,    -3.58773,     2.93905>,      0.37
 texture { colorA }
 }
sphere { 
<     6.36007,    -3.32729,     2.03220>,      0.37795
 texture { colorO }
 }
sphere { 
<     6.36007,    -3.32729,     2.03220>,      0.52859
 texture { colorO }
 }
 cylinder {
<     6.36007,    -3.32729,     2.03220>, <     5.46709,    -3.40585,     2.07861>,      0.37
 texture { colorO }
 }
sphere { 
<     3.84635,    -3.69105,     3.75307>,      0.37795
 texture { colorO }
 }
sphere { 
<     3.84635,    -3.69105,     3.75307>,      0.52859
 texture { colorO }
 }
 cylinder {
<     3.84635,    -3.69105,     3.75307>, <     4.21024,    -3.58773,     2.93905>,      0.37
 texture { colorO }
 }
 }
 object {molecule}
